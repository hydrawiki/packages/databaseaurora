<?php
/**
 * This is the MySQLi database abstraction layer.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Database
 */
namespace Wikimedia\Rdbms;

use InvalidArgumentException;

/**
 * Database abstraction object for PHP extension mysqli with AWS Aurora customizations.
 *
 * @ingroup Database
 * @since 1.3?
 * @see Database
 */
class DatabaseAurora extends DatabaseMysqli {
	/**
	 * @return bool|int
	 */
	protected function getLagFromSlaveStatus() {
		// Call doQuery() directly, to avoid opening a transaction if DBO_TRX is set
		$res = $this->doQuery( "SELECT Replica_lag_in_msec FROM mysql.ro_replica_status WHERE Server_id = @@global.aurora_server_id;" );
		$row = $res ? $this->fetchRow( $res ) : false;
		if ( !$row ) {
			return false;
		}
		// Note that this returns float, while the function this overrides returns int.
		// The GTID version of getLag returns float, so this is acceptable.
		return floatval( $row['Replica_lag_in_msec'] ) / 1000;
	}

	public function masterPosWait( DBMasterPos $pos, $timeout ) {
		if ( !( $pos instanceof MySQLMasterPos ) ) {
			throw new InvalidArgumentException( "Position not an instance of MySQLMasterPos" );
		}

		if ( $this->trxLevel ) {
			throw new DBExpectedError( $this, "Replication wait failed: cannot wait from within transaction" );
		}

		if ( $this->getLBInfo( 'is static' ) === true ) {
			return 0; // this is a copy of a read-only dataset with no master DB
		} elseif ( $this->lastKnownReplicaPos && $this->lastKnownReplicaPos->hasReached( $pos ) ) {
			return 0; // already reached this point for sure
		}

		$start = microtime( true );
		$waitTime = 0;
		// Emulate MySQL MASTER_POS_WAIT behavior to treat a $timeout <= 0 as infinite
		while ( $timeout <= 0 || microtime( true ) - $start < $timeout ) {
			// Give the replica time to catch up, staggered randomly to prevent stampeding the DB.
			// Sleeping before checking is sensible since we're here because either the
			// lastKnownReplicaPos is behind the target pos, or we know we've just written to the
			// master.
			// 5-7 ms for the first check and 21-23 ms for the next check ensures >99% of situations
			// will make 2 or fewer queries, since Aurora replica lag p99 is usually <=25ms.
			// Querying ro_replica_status can be expensive since it's technically a hot table.
			$waitTime = ( $waitTime === 0 ? 5000 : 21000 ) + rand( 0, 2000 );
			usleep( $waitTime );
			// Use the time at query execution for the position
			$now = microtime( true );
			// Call doQuery() directly, to avoid opening a transaction if DBO_TRX is set
			// Get current Lsn
			$res = $this->doQuery( "SELECT IF(Current_read_lsn = 0, Durable_lsn, Current_read_lsn) as Lsn FROM mysql.ro_replica_status WHERE Server_id = @@global.aurora_server_id;" );
			// If this returns nothing, something has gone very wrong:
			$row = $res ? $this->fetchRow( $res ) : false;
			if ( !$row ) {
				throw new DBExpectedError( $this, "Replication wait failed: {$this->lastError()}" );
			}
			// Test if we've reached the master position
			$currentPos = new MySQLMasterPos( "aurora-replication.0/{$row['Lsn']}", $now );
			if ( $currentPos->hasReached( $pos ) ) {
				$row[0] = 0;
				break;
			}
			// Return a timeout if exiting the loop
			$row[0] = -1;
		}

		// Result can be NULL (error), -1 (timeout), or 0+ per the MySQL manual
		$status = ( $row[0] !== null ) ? intval( $row[0] ) : null;
		if ( $status === null ) {
			if ( !$pos->getGTIDs() ) {
				// T126436: jobs programmed to wait on master positions might be referencing
				// binlogs with an old master hostname; this makes MASTER_POS_WAIT() return null.
				// Try to detect this case and treat the replica DB as having reached the given
				// position (any master switchover already requires that the new master be caught
				// up before the switch).
				$replicationPos = $this->getReplicaPos();
				if ( $replicationPos && !$replicationPos->channelsMatch( $pos ) ) {
					$this->lastKnownReplicaPos = $replicationPos;
					$status = 0;
				}
			}
		} elseif ( $status >= 0 ) {
			// Remember that this position was reached to save queries next time
			$this->lastKnownReplicaPos = $pos;
		}

		return $status;
	}

	/**
	 * Get the position of the master from SHOW SLAVE STATUS
	 *
	 * @return MySQLMasterPos|bool
	 */
	public function getReplicaPos() {
		$now = microtime( true ); // as-of-time *before* fetching GTID variables

		$data = $this->getServerRoleStatus( 'SLAVE', __METHOD__ );
		if ( $data && strlen( $data['File'] ) ) {
			return new MySQLMasterPos(
				"{$data['File']}/{$data['Position']}",
				$now
			);
		}

		return false;
	}

	/**
	 * Get the position of the master from SHOW MASTER STATUS
	 *
	 * @return MySQLMasterPos|bool
	 */
	public function getMasterPos() {
		$now = microtime( true ); // as-of-time *before* fetching GTID variables

		$pos = false;

		$data = $this->getServerRoleStatus( 'MASTER', __METHOD__ );
		if ( $data && strlen( $data['File'] ) ) {
			$pos = new MySQLMasterPos( "{$data['File']}/{$data['Position']}", $now );
		}

		return $pos;
	}

	/**
	 * @param string $role One of "MASTER"/"SLAVE"
	 * @param string $fname
	 * @return string[] Latest available server status row
	 */
	protected function getServerRoleStatus( $role, $fname = __METHOD__ ) {
		// Call doQuery() directly, to avoid opening a transaction if DBO_TRX is set
		switch ( $role ) {
			case 'SLAVE':
				$res = $this->doQuery( "SELECT Current_read_lsn as Lsn FROM mysql.ro_replica_status WHERE Server_id = @@global.aurora_server_id;" );
				break;
			case 'MASTER':
				$res = $this->doQuery( "SELECT Durable_lsn as Lsn FROM mysql.ro_replica_status WHERE Session_id = 'MASTER_SESSION_ID';" );
				break;
		}
		$row = $res ? $this->fetchRow( $res ) : false;
		if ( !$row ) {
			return false; //Breaking the rules and RETURNING FALSE to avoid E_NOTICE of an undefined index.
		}
		return [
			'File' => 'aurora-replication.0',
			'Position' => $row['Lsn']
		];
	}
}

class_alias( DatabaseAurora::class, 'DatabaseAurora' );
