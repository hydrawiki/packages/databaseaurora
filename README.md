# DatabaseAurora

## Installation
Add this dependecy to your MediaWiki installation using Composer:

`composer require hydrawiki/databaseaurora`

A stock MediaWiki file will have to be edited to allow this driver to work.

Change in static function `getClass()` in `includes/libs/rdbms/database/Database.php` from:

`'mysql' => [ 'mysqli' => DatabaseMysqli::class ],`,

To:

`'mysql' => [ 'mysqli' => DatabaseMysqli::class, 'aurora' => DatabaseAurora::class ],`